package org.sythe.suf.fractal.view;

import java.awt.Color;

import javax.swing.JFrame;

import org.sythe.suf.fractal.model.FractalSquare;
import org.sythe.suf.fractal.presenter.IFractalPresenter;

public interface IFractalView
{
    IFractalPresenter getPresenter();
    JFrame getFrame();
    int getSelectedRule();
    
    void setPresenter(IFractalPresenter fracalViewPrsenter);
    void setGenerationSlider(int generation);
    void setFractalImage(FractalSquare[] fractal);
    void setVisible(boolean visible);
    void setRuleDisplay(Color[] colors);
    void setRule(int index, Color color);
    void setSelectedRule(int index);
}
