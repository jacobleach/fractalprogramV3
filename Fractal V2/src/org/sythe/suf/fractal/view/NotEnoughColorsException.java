package org.sythe.suf.fractal.view;

public class NotEnoughColorsException extends RuntimeException
{
    public NotEnoughColorsException(String message)
    {
        super(message);
    }
}
