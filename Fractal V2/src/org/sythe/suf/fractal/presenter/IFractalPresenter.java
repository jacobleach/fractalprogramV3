package org.sythe.suf.fractal.presenter;

import java.awt.Color;

import org.sythe.suf.fractal.model.IFractalModel;
import org.sythe.suf.fractal.view.IFractalView;

public interface IFractalPresenter
{
    IFractalModel getModel();
    IFractalView getView();
    int getFractalDisplaySize();
    Color[] getRuleColors();
    Color getRuleColor(int index);

    void setModel(IFractalModel fractalModel);
    void setView(IFractalView fractalView);
    
    void clickedRuleDisplay(int index);
    void clickedRuleSelector(int index);
    void generationChanged(int newGeneration);
    void run();

    
    
    
}
