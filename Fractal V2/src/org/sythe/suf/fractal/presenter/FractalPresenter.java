package org.sythe.suf.fractal.presenter;

import java.awt.Color;

import org.sythe.suf.fractal.components.RuleSelectionDialogue;
import org.sythe.suf.fractal.model.IFractalModel;
import org.sythe.suf.fractal.view.IFractalView;

public class FractalPresenter implements IFractalPresenter
{
	private IFractalModel fractal;
	private IFractalView view;

	public IFractalModel getModel()
	{
		return fractal;
	}

	public IFractalView getView()
	{
		return view;
	}

	public int getFractalDisplaySize()
	{
		return fractal.getDisplaySize();
	}

	public Color[] getRuleColors()
	{
		Color[] temp = new Color[fractal.getNumberOfRules()];
		for (int i = 0; i < temp.length; i++)
		{
			temp[i] = fractal.getRuleColor(i);
		}
		return temp;
	}

	public Color getRuleColor(int index)
	{
		return fractal.getRuleColor(index);
	}

	public void setModel(IFractalModel fractalModel)
	{
		fractal = fractalModel;
	}

	public void setView(IFractalView fractalView)
	{
		view = fractalView;
	}

	public void generationChanged(int newGeneration)
	{
		fractal.setGeneration(newGeneration);
		updateFractal();
	}

	public void clickedRuleDisplay(int index)
	{
		int selection = RuleSelectionDialogue.showDialog(view.getFrame(), getRuleColors());

		fractal.setRule(view.getSelectedRule(), index, selection);
		updateRuleDisplay();
		fractal.reIterate();
		updateFractal();
	}

	public void clickedRuleSelector(int index)
	{
		System.out.println("Clicked: " + index);
		view.setSelectedRule(index);
		view.setRuleDisplay(fractal.getRuleAsColors(index));
	}

	public void run()
	{
		generationChanged(9);
		view.setGenerationSlider(9);
		view.setVisible(true);
		view.setSelectedRule(0);
	}

	private void updateFractal()
	{
		view.setFractalImage(fractal.getFractalSquares());
	}
	
	private void updateRuleDisplay()
	{
		view.setRuleDisplay(fractal.getRuleAsColors(view.getSelectedRule()));
	}
}
