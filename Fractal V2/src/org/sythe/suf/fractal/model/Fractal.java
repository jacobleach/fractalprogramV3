

package org.sythe.suf.fractal.model;

import java.awt.Color;

public class Fractal implements IFractalModel
{
	private FractalRule[] rules;

	private int ruleNumber;
	private int maxGenerations;
	private int displaySize;
	private int generation;
	private int squareNumber;

	private FractalSquare[] squares;
	private FractalSquare seed;

	public Fractal(int ruleNumber, int maxGenerations)
	{
		createRules(ruleNumber);
		setDefaultFractal();
		this.ruleNumber = ruleNumber;
		this.maxGenerations = maxGenerations;
		this.displaySize = (int) Math.pow(2, maxGenerations);

		seed = new FractalSquare(0, 0, displaySize, rules[0]);
		squareNumber = 1;
		squares = new FractalSquare[1];
		generation = 0;

		squares[0] = seed;
	}

	public void reIterate()
	{
		int temp = generation;
		reset();
		setGeneration(temp);
	}
	
	/*
	 * ************************************ Getter Methods ************************************
	 */

	/*
	 * Returns the size in px that the fractal must be to be displayed at maxGeneration
	 */
	public int getDisplaySize()
	{
		return displaySize;
	}

	/*
	 * Returns the current generation of the fractal
	 */
	public int getGeneration()
	{
		return generation;
	}

	/*
	 * Returns the number of rules that exist
	 */
	public int getNumberOfRules()
	{
		return ruleNumber;
	}

	/*
	 * Returns the indexes of the rules assosiated with index's rule
	 */
	public int[] getRule(int index)
	{
		try
		{
			return rules[index].getRuleIndexes();
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			throw new FractalRuleDoesNotExistException("Tried to access FractalRule that does not exist. An index exceeds the number of rules.");
		}
	}

	public Color[] getRuleAsColors(int index)
	{
		return rules[index].getRuleAsColors();
	}

	/*
	 * Returns a copy of the color of the rule at index
	 */
	public Color getRuleColor(int index)
	{
		try
		{
			return rules[index].getColor();
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			throw new FractalRuleDoesNotExistException("Tried to access FractalRule that does not exist. An index exceeds the number of rules.");
		}
	}

	/*
	 * Returns a copy of the array that holds all of the FractalSquares
	 */
	public FractalSquare[] getFractalSquares()
	{
		FractalSquare[] temp = new FractalSquare[squareNumber];
		System.arraycopy(squares, 0, temp, 0, squareNumber);

		return temp;
	}

	/*
	 * ************************************ Setter Methods ************************************
	 */

	/*
	 * Sets the generation of the Fractal
	 */
	public void setGeneration(int generation)
	{
		if (this.generation != generation && maxGenerations >= generation)
		{
			if (this.generation > generation)
			{
				reset();
				iterate(generation);
			}
			else
			{
				iterate(generation - this.generation);
			}

			this.generation = generation;
		}
	}

	/*
	 * Returns the Fractal to its default state
	 */
	public void setToDefault()
	{
		rules = null;
		createRules(ruleNumber);
	}

	/*
	 * Sets the rule at index
	 * Must first convert from indexes to FractalRule objects
	 */
	public void setRule(int index, int[] rule)
	{
		try
		{
			FractalRule[] temp = new FractalRule[rule.length];

			for (int i = 0; i < rule.length; i++)
			{
				temp[i] = rules[rule[i]];
			}

			rules[index].setRule(temp);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			throw new FractalRuleDoesNotExistException("Tried to access FractalRule that does not exist. An index exceeds the number of rules.");
		}
	}

	/**
	 * 
	 * @param fractalIndex
	 *            the rule being changed
	 * @param ruleIndex
	 *            the specific rule quadrant being chaned. 0 = topleft, 1 = topright, 1 =
	 *            bottomleft, 2 = bottomright
	 * @param rule
	 *            the index of the rule the specific rule quadrant should be set to
	 */
	public void setRule(int fractalIndex, int ruleIndex, int rule)
	{
		try
		{
			rules[fractalIndex].setRule(ruleIndex, rules[rule]);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			throw new FractalRuleDoesNotExistException("Tried to access FractalRule that does not exist. An index exceeds the number of rules.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.sythe.suf.fractal.model.IFractalModel#setRuleColor(int, java.awt.Color)
	 */
	public void setRuleColor(int index, Color color)
	{
		try
		{
			rules[index].setColor(color);
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			throw new FractalRuleDoesNotExistException("Tried to access FractalRule that does not exist. An index exceeds the number of rules.");
		}
	}

	/*
	 * ************************************ Helper Methods ************************************
	 */

	/*
	 * Iterates the Fractal times # of times.
	 * Is called by the setGeneration method.
	 */
	private void iterate(int times)
	{
		if (times != 0)
		{
			FractalSquare[] temp = new FractalSquare[(int) (squareNumber * Math.pow(4, times))];
			FractalSquare[] newSquares;
			int oldNumber;
			boolean even;
			int counter = 0;

			// First iterate the current set of squares
			for (int i = 0; i < squareNumber; i++)
			{
				newSquares = squares[i].itterate();
				System.arraycopy(newSquares, 0, temp, counter, newSquares.length);
				counter += newSquares.length;
				// Slower
				// for (int j = 0; j < newSquares.length; j++)
				// {
				// temp[counter++] = newSquares[j];
				// }
			}

			// Then iterate the new squares in temp, times - 1 times.
			for (int i = 1; i < times; i++)
			{
				oldNumber = counter;
				counter = 0;
				even = (i % 2) == 0;
				for (int j = oldNumber; j > 0; j--)
				{
					/*
					 * After a lot of bug testing this works
					 * I do not know why it didn't work before other than
					 * A) It didn't work if the number of times was 3 or 5 for some reason (after I
					 * fixed it a bit)
					 * 
					 * B) It worked after I started getting rid of the odd ones from the side
					 * that the new one was growing on. I believe for large enough n that the old
					 * ones were being overwritten but with small n they did not. I only looked at
					 * small n and not large n. Proving it with math would have helped. ._.
					 */
					newSquares = temp[((even) ? (temp.length - j) : (j - 1))].itterate();
					System.arraycopy(newSquares, 0, temp, ((even) ? (counter) : (temp.length - counter - newSquares.length)), newSquares.length);
					counter += newSquares.length;
					// Much slower
					// for (int k = 0; k < newSquares.length; k++)
					// {
					// temp[((even) ? (counter) : (temp.length - counter - 1))] = newSquares[k];
					// counter++;
					// }
				}
			}

			// Move everything to the front of the array and place nulls in the back
			if (((times - 1) % 2) != 0)
			{
				moveToFront(temp, counter);
			}
			squareNumber = counter;
			squares = temp;
		}
		else
		{
			reset();
		}
	}

	/*
	 * Resets the Fractal to its seed
	 */
	private void reset()
	{
		squares = new FractalSquare[1];
		squareNumber = 1;
		generation = 0;

		squares[0] = seed;
	}

	/*
	 * Creates the initial array of rules for the fractal
	 */
	private void createRules(int number)
	{
		// Quick fix for now
		if (number > 15)
		{
			number = 15;
		}
		rules = new FractalRule[number];
		Color[] colors = getDefaultColors();

		for (int i = 0; i < number; i++)
		{
			rules[i] = new FractalRule(colors[i], i);
		}
	}

	/*
	 * Returns default color array for use in createRules method
	 * 
	 * Todo: Make not just a hardcoded array
	 * Todo: Make it not fail if colors > 12
	 */
	private Color[] getDefaultColors()
	{
		return new Color[] { Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW, Color.BLACK, Color.CYAN, Color.PINK, Color.MAGENTA, Color.ORANGE, Color.GRAY, new Color(150, 75, 0),
				new Color(128, 0, 128), new Color(0, 128, 128), new Color(72, 61, 139), Color.WHITE };
	}

	/*
	 * Sets the default fractal
	 */
	private void setDefaultFractal()
	{
		rules[0].setRule(new FractalRule[] { rules[2] });
		rules[1].setRule(new FractalRule[] { rules[5], rules[1], rules[1], rules[5] });
		rules[2].setRule(new FractalRule[] { rules[2], rules[2], rules[1], rules[2] });
		rules[3].setRule(new FractalRule[] { rules[2], rules[8], rules[8], rules[4] });
		rules[4].setRule(new FractalRule[] { rules[2], rules[1], rules[1], rules[2] });
		rules[5].setRule(new FractalRule[] { rules[3], rules[5], rules[0], rules[3] });
		rules[6].setRule(new FractalRule[] { rules[5], rules[3], rules[1], rules[2] });
		rules[7].setRule(new FractalRule[] { rules[5], rules[3], rules[1], rules[2] });
		rules[8].setRule(new FractalRule[] { rules[8], rules[5], rules[5], rules[8] });
	}

	/*
	 * Arr is an array of Objects that grows from back to front with size elements that contain
	 * data.
	 * 
	 * If arr.length = size or size == 0, the method simply returns the array.
	 * 
	 * Returns arr with the Objects from front to back (order is NOT maintained) with null in place
	 * of moved data.
	 */
	public static Object[] moveToFront(Object[] arr, int size)
	{
		if (arr.length != size && size != 0)
		{
			boolean test = (size * 2) > arr.length;
			int currentLocation;

			for (int i = 0; i < ((test) ? (arr.length - size) : (size)); i++)
			{
				currentLocation = ((test) ? (arr.length - (arr.length - size)) : (arr.length - size)) + i;

				arr[i] = arr[currentLocation];
				arr[currentLocation] = null;
			}
		}
		return arr;
	}

	public static void main(String[] args)
	{
		Fractal a = new Fractal(15, 9);
		long d = System.currentTimeMillis();
		for (int i = 0; i < 10; i++)
		{
			a.setGeneration(i);
		}
		System.out.println("Time: " + (System.currentTimeMillis() - d));
	}
}
