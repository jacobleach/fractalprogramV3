package org.sythe.suf.fractal.model;

import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Float;


public class FractalSquare
{
    private Rectangle2D.Float rectangle;
    private FractalRule squareRule;

    protected FractalSquare(int x, int y, int size, FractalRule rule)
    {
        rectangle = new Rectangle2D.Float(x, y, size, size);
        this.squareRule = rule;
    }

    /*
     * Returns the squares that his square turns into when iterated
     */
    protected FractalSquare[] itterate()
    {
        FractalRule[] rule = squareRule.getRule();

        int x = (int) rectangle.x;
        int y = (int) rectangle.y;
        int half = (int) (rectangle.width / 2);
        FractalSquare[] temp;

        if (rule.length > 1)
        {
            temp = new FractalSquare[4];
            temp[0] = new FractalSquare(x, y, half, rule[0]);
            temp[1] = new FractalSquare(x + half, y, half, rule[1]);
            temp[2] = new FractalSquare(x, y + half, half, rule[2]);
            temp[3] = new FractalSquare(x + half, y + half, half, rule[3]);
        }
        else
        {
            temp = new FractalSquare[1];
            temp[0] = new FractalSquare(x, y, (int) rectangle.width, rule[0]);
        }

        return temp;
    }

    /*
     * Return a copy of the squareRule's color object
     */
    public Color getColor()
    {
        Color c = squareRule.getColor();
        return new Color(c.getRed(), c.getGreen(), c.getBlue());
    }
    
    /*
     * Returns a copy of the rectangle associated with this FractalSquare
     */
    public Rectangle2D.Float getSquare()
    {
        return (Float) rectangle.clone();
    }
}