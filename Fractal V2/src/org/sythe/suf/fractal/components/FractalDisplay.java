package org.sythe.suf.fractal.components;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.sythe.suf.fractal.model.Fractal;
import org.sythe.suf.fractal.model.FractalSquare;

public class FractalDisplay extends JPanel
{
	private BufferedImage image;
	private Graphics2D g2d;

	public FractalDisplay(int size)
	{
		setSize(size, size);
		createEmptyImage();
	}

	/*
	 * Creates the image that the fractal is drawn onto
	 */
	private void createEmptyImage()
	{
		image = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
		g2d = (Graphics2D) image.getGraphics();
	}

	/*
	 * Paints the fractal onto the image and then calls repaint
	 */
	public void paintFractal(FractalSquare[] squares)
	{
		for (int i = 0; i < squares.length; i++)
		{
			g2d.setColor(squares[i].getColor());
			g2d.fill(squares[i].getSquare());
		}
		repaint();
	}

	/*
	 * Paints the image with the fractal to the screen
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		if (image == null)
		{
			createEmptyImage();
		}

		g.drawImage(image, 0, 0, null);

	}

	public static void main(String[] args)
	{
		JFrame a = new JFrame();
		a.setSize(600, 600);
		a.setLayout(null);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		FractalDisplay b = new FractalDisplay(512);
		b.setLocation(0, 0);
		a.add(b);
		a.setVisible(true);
		Fractal c = new Fractal(12, 10);
		c.setGeneration(10);
		b.paintFractal(c.getFractalSquares());
		long keep = System.currentTimeMillis();
		b.paintFractal(c.getFractalSquares());
		System.out.println(System.currentTimeMillis() - keep);
	}
}
