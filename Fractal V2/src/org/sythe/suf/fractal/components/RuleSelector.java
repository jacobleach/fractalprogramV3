package org.sythe.suf.fractal.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class RuleSelector extends JPanel implements ActionListener
{
    private Color INACTIVE_BUTTON_COLOR = Color.gray;

    private int rows;
    private int columns;
    private int buttonWidth = 50;// Default value of 50
    private int buttonHeight = 50;// Default value of 50
    private int padding = 2;// Default value of 2
    private Color[] colors;
    private IndexedButton[] buttons;
    private int activeButtons;

    public RuleSelector(int number, int width, int height, int columns, int rows, Color[] colors)
    {
        activeButtons = number;
        buttons = new IndexedButton[columns * rows];
        this.columns = columns;
        this.rows = rows;
        this.colors = colors;
        this.setLayout(null);
        setSize(columns * (buttonWidth + padding), rows * (buttonHeight + padding));
        createButtons();
    }

    public void setButtonSize(int width, int height)
    {
        buttonWidth = width;
        buttonHeight = height;
    }

    public void setButtonPadding(int pixels)
    {
        padding = pixels;
    }

    public void setColors(Color[] colors)
    {
        this.colors = colors;
    }

    private void createButtons()
    {
        int row = -1;// Needs to be -1 because it incriments right away in the first loop
        int column;
        for (int i = 0; i < columns * rows; i++)
        {
            column = (i % columns);
            if (column == 0)
            {
                row++;
            }
            buttons[i] = new IndexedButton(i, i < activeButtons ? colors[i] : INACTIVE_BUTTON_COLOR);
            buttons[i].setSize(buttonWidth, buttonHeight);
            buttons[i].setLocation((column * buttonWidth) + padding + column, (row * buttonHeight) + padding + row);
            buttons[i].addActionListener(this);
            this.add(buttons[i]);
        }
    }

    public void actionPerformed(ActionEvent e)
    {
        
    }

    public static void main(String[] args)
    {
        JFrame a = new JFrame();
        a.setSize(500, 500);
        a.add(new RuleSelector(12, 50, 50, 5, 4, new Color[] { Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW, Color.BLACK, Color.CYAN, Color.PINK, Color.MAGENTA, Color.ORANGE,
                Color.GRAY, new Color(150, 75, 0), new Color(128, 0, 128), new Color(0, 128, 128), new Color(72, 61, 139), Color.WHITE }));
        a.setVisible(true);
    }
}
