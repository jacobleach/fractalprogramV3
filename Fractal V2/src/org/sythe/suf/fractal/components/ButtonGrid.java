package org.sythe.suf.fractal.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class ButtonGrid extends JPanel
{
	private int columns;
	private int rows;

	private Dimension buttonSize = new Dimension(50, 50);// Default values
	private int padding = 2;// Default = 2

	private IndexedButton[] buttons;

	private final Color DEFAULT_COLOR = Color.GRAY;

	public ButtonGrid(int rows, int columns)
	{
		this.rows = rows;
		this.columns = columns;
		initButtons();
	}

	private void initButtons()
	{
		buttons = new IndexedButton[rows * columns];

		int row = 0;
		int column = 0;

		for (int i = 0; i < buttons.length; i++)
		{
			buttons[i] = new IndexedButton(i, DEFAULT_COLOR);
			buttons[i].setSize(buttonSize.width, buttonSize.height);
			buttons[i].setLocation(getLocation(row, column));
			buttons[i].addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{

				}
			});
			this.add(buttons[i]);

			column = (i % columns);
			row = (column == 0) ? row++ : row;
		}
	}

	/*
	 * ************************************ Getter Methods ************************************
	 */

	public Point getLocation(int row, int column)
	{
		return new Point((column * buttonSize.width) + ((column == 0) ? 0 : padding) + column, (row * buttonSize.height) + ((row == 0) ? 0 : padding) + row);
	}

	public Dimension getPerfectSize()
	{
		Point last = getLocation(rows - 1, columns - 1);

		return new Dimension(last.x + padding + buttonSize.width, last.y + padding + buttonSize.height);
	}

	/**
	 * 
	 * @return a Dimension object that represents the dimensions of the buttons
	 */
	public Dimension getButtonSize()
	{
		return (Dimension) buttonSize.clone();
	}
}
