package org.sythe.suf.fractal.components;

import java.awt.Color;

import javax.swing.JButton;

public class IndexedButton extends JButton
{
    private int index;
    
    public IndexedButton(int index)
    {
    	this(index, Color.lightGray);
    }
    
    public IndexedButton(int index, Color color)
    {
        this.index = index;
        setBackground(color);
    }
    
    public int getIndex()
    {
        return index;
    }
}
